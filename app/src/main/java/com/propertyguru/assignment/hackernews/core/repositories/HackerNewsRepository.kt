package com.propertyguru.assignment.hackernews.core.repositories

import com.propertyguru.assignment.hackernews.data.model.Comment
import com.propertyguru.assignment.hackernews.data.model.Story
import io.reactivex.Single

interface HackerNewsRepository {

    val isNetworkAvailable: Boolean

    fun retrieveStories(): Single<List<Int>>
    fun retrieveStory(id: Int): Single<Story>
    fun retrieveComment(id: Int): Single<Comment>
}