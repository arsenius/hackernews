package com.propertyguru.assignment.hackernews.data.model

import com.squareup.moshi.Json

data class Story(
    @Json(name = "id") override val id: Int,
    @Json(name = "by") val author: String?,
    @Json(name = "title") val title: String?,
    @Json(name = "descendants") val totalComments: Int?,
    @Json(name = "kids") val comments: List<Int>?,
    @Json(name = "score") val score: Int?,
    @Json(name = "time") override val date: Long,
    @Json(name = "type") override val type: String,
    @Json(name = "url") val url: String?,
    @Json(name = "deleted") override val deleted: Boolean?
): Item