package com.propertyguru.assignment.hackernews.core.view_model

import com.airbnb.mvrx.MvRxViewModelFactory
import com.airbnb.mvrx.ViewModelContext
import com.propertyguru.assignment.hackernews.data.state.UiState

class UiViewModel(initialState: UiState): MvRxViewModel<UiState>(initialState) {

    fun saveVisibleItemPosition(position: Int) {
        setState {
            copy(savedVisibleItemPosition = position, scrollToPosition = position == 0,
                canScrollTop = if (position == 0) false else canScrollTop)
        }

    }

    fun onScrollStateChanged(isScrollableToBottom: Boolean, isScrollableToTop: Boolean) = setState {
        copy(canScrollTop = isScrollableToBottom && isScrollableToTop, scrollToPosition = false)
    }


    companion object : MvRxViewModelFactory<UiViewModel, UiState> {

        override fun create(viewModelContext: ViewModelContext, state: UiState): UiViewModel {

            return UiViewModel(state)
        }
    }
}