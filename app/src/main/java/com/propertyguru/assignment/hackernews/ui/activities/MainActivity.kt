package com.propertyguru.assignment.hackernews.ui.activities

import android.os.Bundle
import android.view.MenuItem
import com.airbnb.mvrx.BaseMvRxActivity
import com.propertyguru.assignment.hackernews.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseMvRxActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        navHostFragment.childFragmentManager.fragments.forEach {
            it.onOptionsItemSelected(item)
        }
        return super.onOptionsItemSelected(item)
    }
}
