package com.propertyguru.assignment.hackernews.ui.controllers

import com.airbnb.mvrx.withState
import com.propertyguru.assignment.hackernews.core.view_model.HackerNewsViewModel

class CommentController(viewModel: HackerNewsViewModel): BaseController<Int>(viewModel) {


    override fun buildModels(mainCommentId: Int?) = withState(viewModel) {
        if(isError)
            return@withState

        val highlightedComment = it.comments[mainCommentId ?: 0]?.invoke() ?: return@withState
        renderComment(0, highlightedComment, true)

        if(highlightedComment.replies != null)
            for(commentId in highlightedComment.replies) {
                handleItem(it, it.comments, commentId, { viewModel.populateComment(commentId) }) {comment ->
                    renderComment(commentId, comment)
                }
            }
    }
}