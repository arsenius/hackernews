package com.propertyguru.assignment.hackernews.core.view_model.errors

import android.content.Context
import com.propertyguru.assignment.hackernews.R

class HackerNewsErrorMessagesImpl(private val context: Context): HackerNewsErrorMessages {
    override val uknownError: String
        get() = context.getString(R.string.unknown_error)
    override val whenOffline: String
        get() = context.getString(R.string.when_network_is_not_available)
}