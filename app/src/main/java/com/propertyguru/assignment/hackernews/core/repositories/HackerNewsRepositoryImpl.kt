package com.propertyguru.assignment.hackernews.core.repositories

import android.content.Context
import com.propertyguru.assignment.hackernews.core.api.HackerNewsApi
import com.propertyguru.assignment.hackernews.data.model.Comment
import com.propertyguru.assignment.hackernews.data.model.Story
import io.reactivex.Single
import androidx.core.content.ContextCompat.getSystemService
import android.net.ConnectivityManager



/*
    This is just outline for scalable architecture
 */

class HackerNewsRepositoryImpl(private val context: Context,
                               private val api: HackerNewsApi): HackerNewsRepository {

    override val isNetworkAvailable: Boolean
        get()  {
            val manager = getSystemService(context, ConnectivityManager::class.java)
            val networkInfo = manager?.activeNetworkInfo
            return (networkInfo != null && networkInfo.isConnected)
        }

    override fun retrieveStories(): Single<List<Int>> =
        api.fetchStories()

    override fun retrieveStory(id: Int): Single<Story> =
        api.fetchStory(id)


    override fun retrieveComment(id: Int): Single<Comment> =
            api.fetchComment(id)
}