package com.propertyguru.assignment.hackernews.ui.controllers


import com.airbnb.mvrx.Success
import com.airbnb.mvrx.withState
import com.propertyguru.assignment.hackernews.core.view_model.HackerNewsViewModel


class StoriesController(viewModel: HackerNewsViewModel): BaseController<Unit>(viewModel) {


    override fun buildModels(void: Unit?) = withState(viewModel){
        if(it.allStories !is Success) {
            if(isError)
                return@withState
        }

        val allStories = it.allStories() ?: return@withState

        for(storyId in allStories)
            handleItem(it, it.stories, storyId, { viewModel.populateStory(storyId) }) {story ->
                renderStory(storyId, story)
            }
    }
}