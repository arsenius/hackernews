package com.propertyguru.assignment.hackernews.ui.controllers



import com.airbnb.mvrx.withState
import com.propertyguru.assignment.hackernews.core.view_model.HackerNewsViewModel

class StoryController(viewModel: HackerNewsViewModel): BaseController<Int>(viewModel) {


    override fun buildModels(storyId: Int?) = withState(viewModel){
        if(isError)
            return@withState

        val highlightedStory = it.stories[storyId ?: 0]?.invoke() ?: return@withState
        renderStory(0, highlightedStory, highlighted = true)

        if(highlightedStory.comments != null)
            for(commentId in highlightedStory.comments) {
                handleItem(it, it.comments, commentId, { viewModel.populateComment(commentId) }) {comment ->
                  renderComment(commentId, comment)
                }
            }
    }



}