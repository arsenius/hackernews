package com.propertyguru.assignment.hackernews

import androidx.multidex.MultiDexApplication
import com.airbnb.epoxy.EpoxyAsyncUtil
import com.airbnb.epoxy.EpoxyController
import com.propertyguru.assignment.hackernews.core.di.diModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: MultiDexApplication() {

    private fun initDependencyInjection() {
        startKoin {
            androidContext(this@App)
            modules(diModules(this@App))
        }
    }

    private fun setupEpoxy() {
        val handler = EpoxyAsyncUtil.getAsyncBackgroundHandler()
        EpoxyController.defaultDiffingHandler = handler
        EpoxyController.defaultModelBuildingHandler = handler
    }

    override fun onCreate() {
        super.onCreate()
        initDependencyInjection()
        setupEpoxy()
    }
}