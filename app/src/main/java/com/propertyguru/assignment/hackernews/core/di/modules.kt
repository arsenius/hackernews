package com.propertyguru.assignment.hackernews.core.di

import android.content.Context
import com.propertyguru.assignment.hackernews.core.repositories.HackerNewsRepository
import com.propertyguru.assignment.hackernews.core.repositories.HackerNewsRepositoryImpl
import com.propertyguru.assignment.hackernews.core.retrofit.ApiBuilder
import com.propertyguru.assignment.hackernews.core.view_model.errors.HackerNewsErrorMessages
import com.propertyguru.assignment.hackernews.core.view_model.errors.HackerNewsErrorMessagesImpl
import org.koin.dsl.module

fun diModules(context: Context) = module {
    single<HackerNewsRepository> { HackerNewsRepositoryImpl(context, ApiBuilder(context).build()) }
    single<HackerNewsErrorMessages> { HackerNewsErrorMessagesImpl(context) }
}