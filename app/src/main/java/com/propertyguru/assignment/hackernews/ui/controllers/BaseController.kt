package com.propertyguru.assignment.hackernews.ui.controllers

import android.view.View
import com.airbnb.epoxy.TypedEpoxyController
import com.airbnb.mvrx.Async
import com.airbnb.mvrx.withState
import com.propertyguru.assignment.hackernews.core.view_model.HackerNewsViewModel
import com.propertyguru.assignment.hackernews.core.view_model.getItemStatus
import com.propertyguru.assignment.hackernews.data.model.Comment
import com.propertyguru.assignment.hackernews.data.model.Item
import com.propertyguru.assignment.hackernews.data.model.Story
import com.propertyguru.assignment.hackernews.data.state.HackerNewsState
import com.propertyguru.assignment.hackernews.rowComment
import com.propertyguru.assignment.hackernews.rowError
import com.propertyguru.assignment.hackernews.rowLoading
import com.propertyguru.assignment.hackernews.rowStory

abstract class BaseController<T>(protected val viewModel: HackerNewsViewModel): TypedEpoxyController<T>() {
    protected var onLinkClick: (String) -> Unit = {}
    protected var onItemClick: (Int) -> Unit = {}

    fun setOnLinkClickListener(listener: (url: String) -> Unit) {
        onLinkClick = listener
    }

    fun setOnItemClickListener(listener: (itemId: Int) -> Unit) {
        onItemClick = listener
    }

    private val testDuplicates = HashSet<Int>()


    protected val isError: Boolean
            get() = withState(viewModel) {
                if(!it.mainError.isNullOrEmpty()) {
                    rowError {
                        id(0)
                        error(it.mainError)
                    }
                    true
                }
                else
                    false
            }



    protected fun <T: Item> handleItem(
        state: HackerNewsState,
        items: Map<Int, Async<T>>,
        itemId: Int,
        onPopulate: (itemId: Int) -> Unit,
        onSuccess: (item: T) -> Unit
    ) {
        when(items.getItemStatus(itemId)) {
            HackerNewsViewModel.Status.Success -> {
                val item = items[itemId]?.invoke() ?: return
                if(item.deleted == null)
                    onSuccess(item)
            }
            HackerNewsViewModel.Status.Fail ->
                rowError {
                    id(itemId)
                    error(state.errors[itemId])
                }
            HackerNewsViewModel.Status.Pending ->
                if(state.mainError.isNullOrEmpty()) {
                    rowLoading {
                        id(itemId)
                        onBind { _, _, _ ->
                            onPopulate(itemId)
                        }
                    }
                }
        }
    }

    protected fun renderStory(storyId: Int, story: Story, highlighted: Boolean = false) {
        rowStory {
            id(storyId)
            model(story)
            highlight(highlighted)
            onLinkClick (View.OnClickListener {
                if(!story.url.isNullOrEmpty())
                    onLinkClick(story.url)
            })
            onStoryClick (View.OnClickListener {
                if(!story.comments.isNullOrEmpty() && !highlighted)
                    onItemClick(story.id)
            })
        }
    }

    protected fun renderComment(commentId: Int, comment: Comment, highlighted: Boolean = false) {
        rowComment {
            id(commentId)
            model(comment)
            highlight(highlighted)
            onRepliesClick(View.OnClickListener {
                onItemClick(commentId)
            })
        }
    }
}