package com.propertyguru.assignment.hackernews.data.model

import com.squareup.moshi.Json

data class Comment(
    @Json(name = "id") override val id: Int,
    @Json(name = "by") val author: String?,
    @Json(name = "text") val text: String?,
    @Json(name = "parent") val parent: Int,
    @Json(name = "kids") val replies: List<Int>?,
    @Json(name = "time") override val date: Long,
    @Json(name = "type") override val type: String,
    @Json(name = "deleted") override val deleted: Boolean?
): Item