package com.propertyguru.assignment.hackernews.core.view_model.errors

interface HackerNewsErrorMessages {
    val whenOffline: String
    val uknownError: String
}