package com.propertyguru.assignment.hackernews.core.extensions

import android.text.Html
import android.text.Spanned
import androidx.core.text.HtmlCompat


val String.asSpanned: Spanned
    get() = HtmlCompat.fromHtml(this.trim(), Html.FROM_HTML_MODE_LEGACY)
