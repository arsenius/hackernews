package com.propertyguru.assignment.hackernews.core.extensions

import android.view.View
import com.google.android.material.snackbar.Snackbar

fun View.showErrorMessage(message: String) {
    var snackBar =
        if(tag != null && tag is Snackbar)
            this.tag as Snackbar
        else
            Snackbar
                .make(this, message, Snackbar.LENGTH_LONG)
    if(!snackBar.isShown)
        snackBar.show()
    tag = snackBar
}