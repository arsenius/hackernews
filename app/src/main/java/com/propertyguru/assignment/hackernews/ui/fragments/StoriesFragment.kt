package com.propertyguru.assignment.hackernews.ui.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.Success
import com.airbnb.mvrx.withState
import com.dinuscxj.refresh.RecyclerRefreshLayout

import com.propertyguru.assignment.hackernews.R
import com.propertyguru.assignment.hackernews.ui.controllers.StoriesController
import kotlinx.android.synthetic.main.fragment_content.*
import androidx.core.os.bundleOf
import com.airbnb.mvrx.fragmentViewModel
import com.propertyguru.assignment.hackernews.core.extensions.*
import com.propertyguru.assignment.hackernews.core.view_model.UiViewModel


/**
 * A simple [Fragment] subclass.
 *
 */
class StoriesFragment : BaseFragment(), RecyclerRefreshLayout.OnRefreshListener {

    private lateinit var controller: StoriesController
    override val uiViewModel: UiViewModel by fragmentViewModel()

    override fun invalidate() = withState(hackerNewsViewModel) {
        refresher.setRefreshing(it.allStories is Loading)
        controller.setData(null)
        if(it.allStories is Success && !it.mainError.isNullOrEmpty()) {
            page.showErrorMessage(it.mainError)
            hackerNewsViewModel.clearMainError()
        }
        renderUi()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_content, container, false)
    }

    override fun onRefresh() {
        hackerNewsViewModel.populateStories()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRefresher()
        controller = initController(
            StoriesController(hackerNewsViewModel),
            R.id.action_storiesFragment_to_storyFragment
        ) {
            bundleOf("story_id" to it)
        }
        manageScroll(true)
        loadStories()
        adjustScroll()
        showBackButton = false
    }


    private fun initRefresher() {
        refresher.setOnRefreshListener(this)
    }

    private fun loadStories() = withState(hackerNewsViewModel) {
        if(it.allStories !is Success)
            onRefresh()
    }

}
