package com.propertyguru.assignment.hackernews.ui.fragments

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import android.view.View
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.airbnb.mvrx.BaseMvRxFragment
import com.airbnb.mvrx.Success
import com.airbnb.mvrx.activityViewModel
import com.airbnb.mvrx.withState
import com.propertyguru.assignment.hackernews.R
import com.propertyguru.assignment.hackernews.core.extensions.onScrollChangedState
import com.propertyguru.assignment.hackernews.core.extensions.setCustomDivider
import com.propertyguru.assignment.hackernews.core.view_model.HackerNewsViewModel
import com.propertyguru.assignment.hackernews.core.view_model.UiViewModel
import com.propertyguru.assignment.hackernews.ui.controllers.BaseController
import kotlinx.android.synthetic.main.fragment_content.*


abstract class BaseFragment : BaseMvRxFragment() {

    private val handler = Handler()

    protected abstract val uiViewModel: UiViewModel

    private val navController: NavController by lazy {
        findNavController()
    }

    protected val hackerNewsViewModel: HackerNewsViewModel by activityViewModel()

    protected var showBackButton: Boolean = false
        set(show) {
            val activity = requireActivity()
            if(activity is AppCompatActivity) {
                activity.supportActionBar?.setDisplayHomeAsUpEnabled(show)
            }
            field = show
        }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId ?: return super.onOptionsItemSelected(item!!)) {
            android.R.id.home -> navController.popBackStack()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacksAndMessages(null)
    }

    @SuppressLint("RestrictedApi")
    protected fun manageScroll(showDivider: Boolean = false) {

        fab.setOnClickListener {
            uiViewModel.onScrollStateChanged(
                isScrollableToBottom = true,
                isScrollableToTop = false
            )
            uiViewModel.saveVisibleItemPosition(0)
        }

        body.onScrollChangedState {visiblePosition, isScrollableToBottom, isScrollableToTop: Boolean ->
            uiViewModel.saveVisibleItemPosition(visiblePosition)
            uiViewModel.onScrollStateChanged(isScrollableToBottom, isScrollableToTop)
        }

        if(showDivider)
            body.setCustomDivider()
    }


    protected fun <T, M: BaseController<T>> initController(controller: M, @IdRes action: Int,
                                     initBundle: (itemId: Int) -> Bundle): M {
        body.setController(controller)
        controller.setOnLinkClickListener {
            launchUrl(it)
        }
        controller.setOnItemClickListener {
            navController.navigate(
                action,
                initBundle(it)
            )
        }
        return controller
    }

    protected fun disableRefresher() {
        refresher.isEnabled = false
    }

    private fun launchUrl(url: String) =
        CustomTabsIntent
            .Builder()
            .addDefaultShareMenuItem()
            .setToolbarColor(ContextCompat.getColor(requireContext(), R.color.highlightedColor))
            .build()
            .launchUrl(requireContext(), Uri.parse(url))

    protected fun whenStoriesInState(inState: () -> Unit) = withState(hackerNewsViewModel) {
        if(it.allStories !is Success)
            navController.popBackStack()
        else
            inState()
    }

    protected fun getItemId(item: String): Int {
        val itemId = arguments?.getInt(item) ?: -1
        if(itemId == -1)
            navController.popBackStack()
        return itemId
    }

    protected fun adjustScroll() = withState(uiViewModel) {
        handler.postDelayed({
            body.scrollToPosition(it.savedVisibleItemPosition)
        }, 125)
    }

    protected fun renderUi() =  withState(uiViewModel) {
        if(it.canScrollTop)
            fab.show()
        else
            fab.hide()
        if(it.scrollToPosition) {
            body.scrollToPosition(it.savedVisibleItemPosition)
            uiViewModel.saveVisibleItemPosition(it.savedVisibleItemPosition)
        }
    }
}