package com.propertyguru.assignment.hackernews.ui.decorators

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.view.View


class DividerItemDecoration : RecyclerView.ItemDecoration {

    private var mDivider: Drawable? = null
    private var mPaddingLeft: Int = 0
    private var mPaddingRight: Int = 0

    constructor(divider: Drawable) {
        mDivider = divider
        mPaddingLeft = 0
        mPaddingRight = 0
    }

    constructor(divider: Drawable, paddingLeft: Int, paddingRight: Int) {
        mDivider = divider
        mPaddingLeft = paddingLeft
        mPaddingRight = paddingRight
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        if (mDivider == null) return
        if (parent.getChildAdapterPosition(view) < 1) return

        if (getOrientation(parent) == LinearLayoutManager.VERTICAL) {
            outRect.top = mDivider!!.intrinsicHeight
        } else {
            outRect.left = mDivider!!.intrinsicWidth
        }
    }

    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        if (mDivider == null) {
            super.onDrawOver(c, parent, state)
            return
        }

        if (getOrientation(parent) == LinearLayoutManager.VERTICAL) {
            val left = parent.paddingLeft + mPaddingLeft
            val right = parent.width - mPaddingRight
            val childCount = parent.childCount

            for (i in 1 until childCount) {
                val child = parent.getChildAt(i)
                val params = child.layoutParams as RecyclerView.LayoutParams
                val size = mDivider!!.intrinsicHeight
                val top = child.top - params.topMargin
                val bottom = top + size
                mDivider!!.setBounds(left, top, right, bottom)
                mDivider!!.draw(c)
            }

        } else {
            val top = parent.paddingTop
            val bottom = parent.height - parent.paddingBottom
            val childCount = parent.childCount

            for (i in 1 until childCount) {
                val child = parent.getChildAt(i)
                val params = child.layoutParams as RecyclerView.LayoutParams
                val size = mDivider!!.intrinsicWidth
                val left = child.left - params.leftMargin
                val right = left + size
                mDivider!!.setBounds(left, top, right, bottom)
                mDivider!!.draw(c)
            }
        }
    }

    private fun getOrientation(parent: RecyclerView): Int {
        if (parent.layoutManager is LinearLayoutManager) {
            val layoutManager = parent.layoutManager as LinearLayoutManager?
            return layoutManager!!.orientation
        } else
            throw IllegalStateException("DividerItemDecoration can only be used with a LinearLayoutManager.")
    }
}