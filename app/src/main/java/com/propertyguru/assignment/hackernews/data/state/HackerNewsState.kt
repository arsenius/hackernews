package com.propertyguru.assignment.hackernews.data.state

import com.airbnb.mvrx.Async
import com.airbnb.mvrx.MvRxState
import com.airbnb.mvrx.Uninitialized
import com.propertyguru.assignment.hackernews.data.model.Comment
import com.propertyguru.assignment.hackernews.data.model.Story
import java.util.*

data class HackerNewsState(
    val allStories: Async<List<Int>> = Uninitialized,
    val stories: Map<Int, Async<Story>> = emptyMap(),
    val comments: Map<Int, Async<Comment>> = emptyMap(),
    val mainError: String? = null,
    val errors: Map<Int, String> = emptyMap(),
    val stateId: UUID = UUID.randomUUID()
): MvRxState