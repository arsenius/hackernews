package com.propertyguru.assignment.hackernews.core.retrofit

import android.content.Context
import com.propertyguru.assignment.hackernews.R
import com.propertyguru.assignment.hackernews.core.retrofit.adapters.TimestampToLocalDateTimeAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

class ApiBuilder(val context: Context) {

    private val moshi: Moshi
        get() = Moshi.Builder()
            .add(TimestampToLocalDateTimeAdapter)
            .add(KotlinJsonAdapterFactory())
            .build()
    private val client: OkHttpClient
        get() = OkHttpClient.Builder().build()

    private val retrofit: Retrofit
        get() = Retrofit.Builder()
            .client(client)
            .baseUrl(context.getString(R.string.api_base_url))
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()

    @Suppress("NON_PUBLIC_CALL_FROM_PUBLIC_INLINE")
    inline fun <reified T> build() =
        retrofit.create(T::class.java)
}