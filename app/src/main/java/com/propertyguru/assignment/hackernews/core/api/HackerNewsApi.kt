package com.propertyguru.assignment.hackernews.core.api

import com.propertyguru.assignment.hackernews.data.model.Comment
import com.propertyguru.assignment.hackernews.data.model.Story
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface HackerNewsApi {
    @GET("topstories.json")
    fun fetchStories(): Single<List<Int>>

    @GET("item/{id}.json")
    fun fetchStory(@Path("id") id: Int): Single<Story>

    @GET("item/{id}.json")
    fun fetchComment(@Path("id") id: Int): Single<Comment>
}