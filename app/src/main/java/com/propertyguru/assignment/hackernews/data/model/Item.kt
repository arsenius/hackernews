package com.propertyguru.assignment.hackernews.data.model

interface Item {
    val id: Int
    val date: Long
    val type: String
    val deleted: Boolean?
}