package com.propertyguru.assignment.hackernews.data.state

import com.airbnb.mvrx.MvRxState

data class UiState (
    val savedVisibleItemPosition: Int = 0,
    val canScrollTop: Boolean = false,
    val scrollToPosition: Boolean = false
): MvRxState