package com.propertyguru.assignment.hackernews.core.retrofit.adapters

import com.squareup.moshi.FromJson
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId


object TimestampToLocalDateTimeAdapter {
    @FromJson
    fun fromJson(timeStamp: Long): LocalDateTime = LocalDateTime.ofInstant(
            Instant.ofEpochSecond(timeStamp),
            ZoneId.systemDefault())
}