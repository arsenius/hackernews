package com.propertyguru.assignment.hackernews.core.data_binding

import androidx.databinding.BindingAdapter
import com.github.curioustechizen.ago.RelativeTimeTextView
import android.graphics.Typeface
import android.text.method.LinkMovementMethod
import android.widget.TextView
import com.propertyguru.assignment.hackernews.core.extensions.asSpanned


@BindingAdapter("referenceTime")
fun setReferenceTime(view: RelativeTimeTextView, refTime: Long) {
    view.setReferenceTime(refTime*1000L)
}

@BindingAdapter("typeface")
fun setTypeface(v: TextView, style: String) {
    when (style) {
        "bold" -> v.setTypeface(null, Typeface.BOLD)
        "italic" -> v.setTypeface(null, Typeface.ITALIC)
        else -> v.setTypeface(null, Typeface.NORMAL)
    }
}

@BindingAdapter("parseHtml")
fun parseHtml(v: TextView, html: String) {
    if(html.trim().isNotEmpty()) {
        v.text = html.trim().asSpanned
        v.movementMethod = LinkMovementMethod.getInstance()
        v.linksClickable = true
    }

}