package com.propertyguru.assignment.hackernews.core.view_model

import com.airbnb.mvrx.*
import com.propertyguru.assignment.hackernews.core.extensions.copy
import com.propertyguru.assignment.hackernews.core.repositories.HackerNewsRepository
import com.propertyguru.assignment.hackernews.core.view_model.errors.HackerNewsErrorMessages
import com.propertyguru.assignment.hackernews.data.model.Item
import com.propertyguru.assignment.hackernews.data.state.HackerNewsState
import org.koin.android.ext.android.inject
import java.util.*

class HackerNewsViewModel(
    initialState: HackerNewsState,
    private val repository: HackerNewsRepository,
    private val errorMessages: HackerNewsErrorMessages
    ): MvRxViewModel<HackerNewsState>(initialState) {

    enum class Status {
        Success,
        Fail,
        Pending
    }

    private val isOnline: Boolean
        get() {
            val isOnline = repository.isNetworkAvailable

            setState {
                if(!isOnline)
                    copy(mainError = errorMessages.whenOffline, stateId = UUID.randomUUID())
                else
                    copy(mainError = null)
            }

            return isOnline
        }

    fun populateStories() {
        if(isOnline) {
            repository.retrieveStories().execute {
                copy(allStories = it,
                    stories = emptyMap(),
                    comments = emptyMap(),
                    errors = emptyMap(),
                    mainError = if(it is Fail)  it.error.message ?: errorMessages.uknownError else null)
            }
        }
    }

    fun populateStory(storyId: Int) {
        if(isOnline)
            repository.retrieveStory(storyId).execute {
                copy(stories = stories.copy(storyId to it), errors = errors.setIfError(storyId, it))
            }

    }

    fun populateComment(commentId: Int) {
        if(isOnline)
            repository.retrieveComment(commentId).execute {
                copy(comments = comments.copy(commentId to it), errors = errors.setIfError(commentId, it))
            }
    }

    fun clearMainError() = setState {
        copy(mainError = null)
    }

    private fun <T> Map<Int, String>.setIfError(id: Int, it: Async<T>) =
        if(it is Fail)
            this.copy(id to (it.error.message ?: errorMessages.uknownError))
        else
            this


    companion object : MvRxViewModelFactory<HackerNewsViewModel, HackerNewsState> {

        override fun create(viewModelContext: ViewModelContext, state: HackerNewsState): HackerNewsViewModel {
            val activity = viewModelContext.activity
            val repository: HackerNewsRepository by activity.inject()
            val errorMessages: HackerNewsErrorMessages by activity.inject()
            return HackerNewsViewModel(state, repository,
                errorMessages)
        }
    }
}

fun <T: Item> Map<Int, Async<T>>.getItemStatus(itemId: Int): HackerNewsViewModel.Status {
    return when {
        this[itemId] is Success -> HackerNewsViewModel.Status.Success
        this[itemId] is Fail -> HackerNewsViewModel.Status.Fail
        this[itemId] is Loading -> HackerNewsViewModel.Status.Pending
        else -> HackerNewsViewModel.Status.Pending
    }
}
