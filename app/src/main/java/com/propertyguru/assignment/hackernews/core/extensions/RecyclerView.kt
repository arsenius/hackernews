package com.propertyguru.assignment.hackernews.core.extensions

import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.propertyguru.assignment.hackernews.R
import com.propertyguru.assignment.hackernews.ui.decorators.DividerItemDecoration



fun RecyclerView.onScrollChangedState(onStateChanged: (visiblePosition: Int,
                                     isScrollableToBottom: Boolean,
                                     isScrollableToTop: Boolean) -> Unit) {
    addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            onStateChanged(
                    (this@onScrollChangedState
                        .layoutManager as LinearLayoutManager)
                        .findLastCompletelyVisibleItemPosition(),
                    recyclerView.canScrollVertically(1),
                    recyclerView.canScrollVertically(-1)
                )
        }
    })
}

fun RecyclerView.setCustomDivider() {
    val dividerItemDecoration = DividerItemDecoration(
        ContextCompat.getDrawable(context, R.drawable.dotted_line)!!,
        64, 64
    )
    addItemDecoration(dividerItemDecoration)
}