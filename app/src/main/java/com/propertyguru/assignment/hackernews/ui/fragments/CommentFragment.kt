package com.propertyguru.assignment.hackernews.ui.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import com.airbnb.mvrx.fragmentViewModel

import com.propertyguru.assignment.hackernews.R
import com.propertyguru.assignment.hackernews.core.view_model.UiViewModel
import com.propertyguru.assignment.hackernews.ui.controllers.CommentController

/**
 * A simple [Fragment] subclass.
 *
 */
class CommentFragment : BaseFragment() {

    private lateinit var controller: CommentController
    override val uiViewModel: UiViewModel by fragmentViewModel()

    override fun invalidate() {
        whenStoriesInState {
            controller.setData(getItemId("comment_id"))
        }
        renderUi()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_content, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        disableRefresher()
        controller = initController(
            CommentController(hackerNewsViewModel),
            R.id.commentFragment
        ) {
            bundleOf("comment_id" to it)
        }
        manageScroll()
        adjustScroll()
        showBackButton = true
    }

}
