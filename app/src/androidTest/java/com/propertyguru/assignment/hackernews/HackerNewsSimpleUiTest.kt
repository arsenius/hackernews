package com.propertyguru.assignment.hackernews




import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.airbnb.epoxy.EpoxyRecyclerView
import com.propertyguru.assignment.hackernews.ui.activities.MainActivity

import org.junit.Test
import org.junit.runner.RunWith
import org.junit.Rule




/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class HackerNewsSimpleUiTest {

    @get:Rule
    open val activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(
        MainActivity::class.java,
        true,
        true
    )


    @Test
    fun testScrollUpButtonInitialState() {
        onView(withId(R.id.fab)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.INVISIBLE)))
    }

    @Test
    fun testScrollUpButtonWhenScroll() {
        onView(withId(R.id.body)).perform(ViewActions.swipeUp())

        onView(withId(R.id.fab)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))
    }

    @Test
    fun testScrollUpButtonWhenScrollToBottom() {
        val body = activityRule.activity.findViewById<EpoxyRecyclerView>(R.id.body)
        onView(withId(R.id.body)).perform(
            RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(body.adapter!!.itemCount-1)
        )

        onView(withId(R.id.fab)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.INVISIBLE)))
    }

    @Test
    fun testScrollUpButtonWhenScrollToTop() {
        onView(withId(R.id.body)).perform(
            RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0)
        )

        onView(withId(R.id.fab)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.INVISIBLE)))
    }


}
