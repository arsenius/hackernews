package com.propertyguru.assignment.hackernews.mocks

import com.propertyguru.assignment.hackernews.core.repositories.HackerNewsRepository
import com.propertyguru.assignment.hackernews.data.comments
import com.propertyguru.assignment.hackernews.data.model.Comment
import com.propertyguru.assignment.hackernews.data.model.Story
import com.propertyguru.assignment.hackernews.data.rootStories
import com.propertyguru.assignment.hackernews.data.stories
import io.reactivex.Single
import io.reactivex.SingleEmitter
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


class MockHackerNewsRepository: HackerNewsRepository {

    private val delay = 250L
    private var isConnectedToNetwork: Boolean = true
    private var isError: Boolean = false
    private val executor = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors())
    val awaitMilliseconds = delay + 100

    fun simmulateOffline() {
        isConnectedToNetwork = false
    }

    fun simmulateOnline() {
        isConnectedToNetwork = true
    }

    fun simmulateError() {
        isError = true
    }

    fun simmulateNoError() {
        isError = false
    }

    override val isNetworkAvailable: Boolean
        get() = isConnectedToNetwork

    override fun retrieveStories(): Single<List<Int>> {

        val handler = { emitter: SingleEmitter<List<Int>> ->

            val future = executor.schedule({
                if(isError)
                    emitter.onError(Throwable("Some error"))
                else
                    emitter.onSuccess(rootStories)
            }, delay, TimeUnit.MILLISECONDS)

            emitter.setCancellable { future.cancel(false) }
        }

        return  Single.create(handler)
    }

    override fun retrieveStory(storyId: Int): Single<Story> {

        val handler = { emitter: SingleEmitter<Story> ->

            val future = executor.schedule({
                val story = stories[storyId]
                when {
                    story != null -> emitter.onSuccess(story)
                    storyId >= 0 -> emitter.onSuccess(Story(
                        id = 0,
                        author = null,
                        title = null,
                        totalComments = null,
                        comments = null,
                        score = null,
                        date = 0,
                        type = "story",
                        url = null,
                        deleted = true
                    ))
                    else -> emitter.onError(Throwable("Some story error"))
                }

            }, delay, TimeUnit.MILLISECONDS)

            emitter.setCancellable { future.cancel(false) }
        }

        return  Single.create(handler)
    }

    override fun retrieveComment(commentId: Int): Single<Comment> {
        val handler = { emitter: SingleEmitter<Comment> ->

            val future = executor.schedule({
                val comment = comments[commentId]
                when {
                    comment != null -> emitter.onSuccess(comment)
                    commentId >= 0 -> emitter.onSuccess(Comment(
                        id = 0,
                        author = null,
                        text = null,
                        parent = 0,
                        replies = null,
                        date = 0,
                        type = "comment",
                        deleted = true
                    ))
                    else -> emitter.onError(Throwable("Some comment error"))
                }

            }, delay, TimeUnit.MILLISECONDS)

            emitter.setCancellable { future.cancel(false) }
        }

        return  Single.create(handler)
    }
}