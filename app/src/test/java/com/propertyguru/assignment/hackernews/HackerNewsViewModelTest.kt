package com.propertyguru.assignment.hackernews

import com.airbnb.mvrx.Fail
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.Success
import com.airbnb.mvrx.withState
import com.propertyguru.assignment.hackernews.core.view_model.HackerNewsViewModel
import com.propertyguru.assignment.hackernews.core.view_model.UiViewModel
import com.propertyguru.assignment.hackernews.core.view_model.errors.HackerNewsErrorMessages
import com.propertyguru.assignment.hackernews.core.view_model.getItemStatus
import com.propertyguru.assignment.hackernews.data.comments
import com.propertyguru.assignment.hackernews.data.rootStories
import com.propertyguru.assignment.hackernews.data.state.HackerNewsState
import com.propertyguru.assignment.hackernews.data.state.UiState
import com.propertyguru.assignment.hackernews.data.stories
import com.propertyguru.assignment.hackernews.di.testModules
import com.propertyguru.assignment.hackernews.mocks.MockHackerNewsRepository
import org.awaitility.kotlin.await
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.koin.core.context.startKoin
import org.koin.test.AutoCloseKoinTest
import org.koin.test.inject
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class HackerNewsViewModelTest: AutoCloseKoinTest() {

    private val mockHackerNewsRepository by inject<MockHackerNewsRepository>()
    private val mockHackerNewsErrorMessages by inject<HackerNewsErrorMessages>()
    private lateinit var viewModel: HackerNewsViewModel
    private lateinit var uiViewModel: UiViewModel


    @Before
    fun setupTest() {
        startKoin {
            modules(testModules)
        }
    }

    @Test
    fun loadTopStories_success() {
        viewModel = HackerNewsViewModel(
            HackerNewsState(),
            mockHackerNewsRepository,
            mockHackerNewsErrorMessages
        )
        viewModel.populateStories()
        Thread.sleep(25)
        withState(viewModel) {
            assertEquals(it.allStories is Loading, true)
        }
        await.atMost(mockHackerNewsRepository.awaitMilliseconds, TimeUnit.MILLISECONDS).until {
            withState(viewModel) {
                if(it.allStories is Success) {
                    it.allStories()?.forEachIndexed { index, storyId ->
                        if(rootStories[index] != storyId)
                            return@withState false
                    } ?: return@withState false
                    return@withState true
                } else
                    return@withState false
            }
        }
    }


    @Test
    fun load25Stories_success() {
        viewModel = HackerNewsViewModel(
            HackerNewsState(),
            mockHackerNewsRepository,
            mockHackerNewsErrorMessages
        )
         rootStories.subList(0, 25).forEach { storyId ->
             viewModel.populateStory(storyId)
             Thread.sleep(25)
             withState(viewModel) {
                 assertEquals(it.stories[storyId] is Loading, true)
             }
             await.atMost(mockHackerNewsRepository.awaitMilliseconds, TimeUnit.MILLISECONDS).until {
                 withState(viewModel) {
                     if(it.stories[storyId] is Success) {
                         val story = it.stories[storyId]?.invoke() ?: return@withState false
                         return@withState story == stories[storyId]
                     } else
                         return@withState false
                 }
             }


        }
    }

    @Test
    fun loadCommentsOf10Stories_success() {
        viewModel = HackerNewsViewModel(
            HackerNewsState(),
            mockHackerNewsRepository,
            mockHackerNewsErrorMessages
        )
        rootStories.subList(0, 10).forEach { storyId ->
            stories[storyId]?.comments?.forEach {commentId ->
                viewModel.populateComment(commentId)
                Thread.sleep(25)
                withState(viewModel) {
                    assertEquals(it.comments[commentId] is Loading, true)
                }
                await.atMost(mockHackerNewsRepository.awaitMilliseconds, TimeUnit.MILLISECONDS).until {
                    withState(viewModel) {
                        if(it.comments[commentId] is Success) {
                            val comment = it.comments[commentId]?.invoke() ?: return@withState false
                            return@withState comment == comments[commentId]
                        } else
                            return@withState false
                    }
                }
            }
        }
    }

    @Test
    fun loadRepliesOfComments_success() {
        viewModel = HackerNewsViewModel(
            HackerNewsState(),
            mockHackerNewsRepository,
            mockHackerNewsErrorMessages
        )
        rootStories.subList(0, 10).forEach { storyId ->
            stories[storyId]?.comments?.forEach { commentId ->
                comments[commentId]?.replies?.forEach { replyId ->
                    viewModel.populateComment(replyId)
                    Thread.sleep(25)
                    withState(viewModel) {
                        assertEquals(it.comments[replyId] is Loading, true)
                    }
                    await.atMost(mockHackerNewsRepository.awaitMilliseconds, TimeUnit.MILLISECONDS).until {
                        withState(viewModel) {
                            if(it.comments[replyId] is Success) {
                                val comment = it.comments[replyId]?.invoke() ?: return@withState false
                                return@withState comment == comments[replyId]
                            } else
                                return@withState false
                        }
                    }
                }
            }
        }
    }

    @Test
    fun loadTopStories_fail() {
        mockHackerNewsRepository.simmulateError()
        viewModel = HackerNewsViewModel(
            HackerNewsState(),
            mockHackerNewsRepository,
            mockHackerNewsErrorMessages
        )
        viewModel.populateStories()
        Thread.sleep(25)
        withState(viewModel) {
            assertEquals(it.allStories is Loading, true)
        }
        await.atMost(mockHackerNewsRepository.awaitMilliseconds, TimeUnit.MILLISECONDS).until {
            withState(viewModel) {
                it.allStories is Fail && it.mainError == "Some error"
            }
        }
        mockHackerNewsRepository.simmulateNoError()
    }

    @Test
    fun loadStory_fail() {
        viewModel = HackerNewsViewModel(
            HackerNewsState(),
            mockHackerNewsRepository,
            mockHackerNewsErrorMessages
        )
        val storyId = -1
        viewModel.populateStory(storyId)
        Thread.sleep(25)
        withState(viewModel) {
            assertEquals(it.stories[storyId] is Loading, true)
        }
        await.atMost(mockHackerNewsRepository.awaitMilliseconds, TimeUnit.MILLISECONDS).until {
            withState(viewModel) {
                it.stories[storyId] is Fail && it.errors[storyId] == "Some story error"
            }
        }
    }


    @Test
    fun loadComment_fail() {
        viewModel = HackerNewsViewModel(
            HackerNewsState(),
            mockHackerNewsRepository,
            mockHackerNewsErrorMessages
        )
        val commentId = -1
        viewModel.populateComment(commentId)
        Thread.sleep(25)
        withState(viewModel) {
            assertEquals(it.comments[commentId] is Loading, true)
        }
        await.atMost(mockHackerNewsRepository.awaitMilliseconds, TimeUnit.MILLISECONDS).until {
            withState(viewModel) {
                it.comments[commentId] is Fail && it.errors[commentId] == "Some comment error"
            }
        }
    }

    @Test
    fun loadStoriesIfOffline_test() {
        mockHackerNewsRepository.simmulateOffline()
        viewModel = HackerNewsViewModel(
            HackerNewsState(),
            mockHackerNewsRepository,
            mockHackerNewsErrorMessages
        )
        viewModel.populateStories()
        Thread.sleep(25)
        withState(viewModel) {
            assertEquals(it.mainError, "Oops! It seems that you are offline. Please check your internet connection.")
        }
        mockHackerNewsRepository.simmulateOnline()
    }

    @Test
    fun loadStoryIfOffline_test() {
        mockHackerNewsRepository.simmulateOffline()
        viewModel = HackerNewsViewModel(
            HackerNewsState(),
            mockHackerNewsRepository,
            mockHackerNewsErrorMessages
        )
        viewModel.populateStory(rootStories[Random().nextInt(rootStories.size)])
        Thread.sleep(25)
        withState(viewModel) {
            assertEquals(it.mainError, "Oops! It seems that you are offline. Please check your internet connection.")
        }
        mockHackerNewsRepository.simmulateOnline()
    }



    @Test
    fun loadCommentIfOffline_test() {
        mockHackerNewsRepository.simmulateOffline()
        viewModel = HackerNewsViewModel(
            HackerNewsState(),
            mockHackerNewsRepository,
            mockHackerNewsErrorMessages
        )
        viewModel.populateComment(comments.keys.first())
        Thread.sleep(25)
        withState(viewModel) {
            assertEquals(it.mainError, "Oops! It seems that you are offline. Please check your internet connection.")
        }
        mockHackerNewsRepository.simmulateOnline()
    }


    @Test
    fun verifyStatusOfLoadingStory() {
        viewModel = HackerNewsViewModel(
            HackerNewsState(),
            mockHackerNewsRepository,
            mockHackerNewsErrorMessages
        )
        val storyId = rootStories[Random().nextInt(rootStories.size)]
        viewModel.populateStory(storyId)
        Thread.sleep(25)
        withState(viewModel) {
            assertEquals(it.stories.getItemStatus(storyId), HackerNewsViewModel.Status.Pending)
        }
        await.atMost(mockHackerNewsRepository.awaitMilliseconds, TimeUnit.MILLISECONDS).until {
            withState(viewModel) {
                it.stories.getItemStatus(storyId) == HackerNewsViewModel.Status.Success
            }
        }
        viewModel.populateStory(-1)
        Thread.sleep(25)
        withState(viewModel) {
            assertEquals(it.stories.getItemStatus(-1), HackerNewsViewModel.Status.Pending)
        }
        await.atMost(mockHackerNewsRepository.awaitMilliseconds, TimeUnit.MILLISECONDS).until {
            withState(viewModel) {
                it.stories.getItemStatus(-1) == HackerNewsViewModel.Status.Fail
            }
        }

    }


    @Test
    fun clearMainError_test() {
        mockHackerNewsRepository.simmulateOffline()
        viewModel = HackerNewsViewModel(
            HackerNewsState(),
            mockHackerNewsRepository,
            mockHackerNewsErrorMessages
        )
        viewModel.populateStories()
        Thread.sleep(25)
        withState(viewModel) {
            assertEquals(!it.mainError.isNullOrEmpty(), true)
        }
        viewModel.clearMainError()
        Thread.sleep(25)
        withState(viewModel) {
            assertEquals(it.mainError.isNullOrEmpty(), true)
        }
        mockHackerNewsRepository.simmulateOnline()
    }

    @Test
    fun saveVisibleStoryPosition_test() {
        uiViewModel = UiViewModel(
            UiState()
        )
        val position = Random().nextInt(rootStories.size)
        uiViewModel.saveVisibleItemPosition(position)
        Thread.sleep(25)
        withState(uiViewModel) {
            assertEquals(it.savedVisibleItemPosition, position)
        }

    }

    @Test
    fun canScrollTop_test() {
        uiViewModel = UiViewModel(
            UiState()
        )
        withState(uiViewModel) {
            assertEquals(it.canScrollTop, false)
        }

        uiViewModel.onScrollStateChanged(
            isScrollableToBottom = false,
            isScrollableToTop = false)
        Thread.sleep(25)
        withState(uiViewModel) {
            assertEquals(it.canScrollTop, false)
        }

        uiViewModel.onScrollStateChanged(
            isScrollableToBottom = true,
            isScrollableToTop = false)
        Thread.sleep(25)
        withState(uiViewModel) {
            assertEquals(it.canScrollTop, false)
        }

        uiViewModel.onScrollStateChanged(
            isScrollableToBottom = false,
            isScrollableToTop = true)
        Thread.sleep(25)
        withState(uiViewModel) {
            assertEquals(it.canScrollTop, false)
        }

        uiViewModel.onScrollStateChanged(
            isScrollableToBottom = true,
            isScrollableToTop = true)
        Thread.sleep(25)
        withState(uiViewModel) {
            assertEquals(it.canScrollTop, true)
        }

    }


    @Test
    fun scrollToPosition_test() {
        uiViewModel = UiViewModel(
            UiState()
        )
        withState(uiViewModel) {
            assertEquals(it.scrollToPosition, false)
        }

        uiViewModel.saveVisibleItemPosition(1)
        Thread.sleep(25)
        withState(uiViewModel) {
            assertEquals(it.scrollToPosition, false)
        }

        uiViewModel.saveVisibleItemPosition(0)
        Thread.sleep(25)
        withState(uiViewModel) {
            assertEquals(it.scrollToPosition, true)
        }
    }

}
