package com.propertyguru.assignment.hackernews.mocks

import com.propertyguru.assignment.hackernews.core.view_model.errors.HackerNewsErrorMessages

class MockHackerNewsErrorMessages: HackerNewsErrorMessages {
    override val whenOffline: String = "Oops! It seems that you are offline. Please check your internet connection."
    override val uknownError: String = "Oops! Something went wrong."
}