package com.propertyguru.assignment.hackernews.di

import com.propertyguru.assignment.hackernews.core.view_model.errors.HackerNewsErrorMessages
import com.propertyguru.assignment.hackernews.mocks.MockHackerNewsErrorMessages
import com.propertyguru.assignment.hackernews.mocks.MockHackerNewsRepository
import org.koin.dsl.module

val testModules = module {
    single { MockHackerNewsRepository() }
    single<HackerNewsErrorMessages> { MockHackerNewsErrorMessages() }
}