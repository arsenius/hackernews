# Hacker News
### Features
- Kotlin only
- Clean, reusable code
- [MvRx](https://github.com/airbnb/MvRx "MvRx") and [Epoxy](https://github.com/airbnb/epoxy "Epoxy") frameworks
- Unidirectional data flow
- [Single Activity app architecture](https://www.reddit.com/r/androiddev/comments/8i73ic/its_official_google_officially_recommends_singl/ "Single Activity app architecture")
- Modern [Android Jetpack](https://developer.android.com/jetpack "Android Jetpack") utilization
- Retain all states (except when app get killed by Android in the background due resource allocation)
- Multiple levels of reply 
- Support 16 Android API and above
- Unit tests and little UI tests (main logic is covered by tests)

### Requirements
- Android Studio 3.3 and above

Current implementation focus on cycle reactive approach when user dispatch actions, view model change the state and view (activity/fragments)  render changes based on the state.

ViewModel contains most of the logic View only reflect the state of the application and dispatch actions produced by UI events from user.  
So the main core is covered by tests (should be about 80% coverage of all logic).

Also I would like to mention perfomance. App is very reponsive and load content relatively fast depending on the internet speed.